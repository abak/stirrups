// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Simple Paintable
 *
 * GdkPaintable is an interface used by GTK for drawings of any sort
 * that do not require layouting or positioning.
 *
 * This demo code gives a simple example on how a paintable can
 * be created.
 *
 * Paintables can be used in many places inside GTK widgets, but the
 * most common usage is inside GtkImage and that's what we're going
 * to do here.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, GObject, Graphene, Gtk } = imports.gi;

const RADIUS = 0.3;

const NuclearIcon = GObject.registerClass({
    Implements: [Gdk.Paintable],
}, class NuclearIcon extends GObject.Object {
    // The flags are very useful to let GTK know that this image is never going to change.
    // This allows many optimizations and should therefore always be set.
    vfunc_get_flags() {
        return Gdk.PaintableFlags.CONTENTS | Gdk.PaintableFlags.SIZE;
    }

    // The snapshot function is the only function we need to implement.
    // It does the actual drawing of the paintable.
    vfunc_snapshot(snapshot, width, height) {
        snapshot.append_color(
            new Gdk.RGBA({ red: 0.9, green: 0.75, blue: 0.15, alpha: 1.0 }),
            new Graphene.Rect({ size: { width, height } })
        );

        let min = Math.min(width, height);
        let cr = snapshot.append_cairo(
            new Graphene.Rect().init((width - min) / 2, (height - min) / 2, min, min)
        );

        cr.translate(width / 2, height / 2);
        cr.scale(min, min);

        cr.arc(0, 0, 0.1, -Math.PI, Math.PI);
        cr.fill();

        cr.setLineWidth(RADIUS);
        cr.setDash([RADIUS * Math.PI / 3], 0);
        cr.arc(0, 0, RADIUS, -Math.PI, Math.PI);
        cr.stroke();

        cr.$dispose();
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    new Gtk.Window({
        application, title: "Nuclear Icon",
        defaultWidth: 300, defaultHeight: 200,
        child: new Gtk.Image({ paintable: new NuclearIcon() }),
    }).present();
});

application.run([]);
