// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Builder
 *
 * Demonstrates a traditional interface, loaded from a XML description,
 * and shows how to connect actions to the menu items and toolbar buttons.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, GLib, GObject, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');

// Register the widget class to instantiate in the builder file.
GObject.registerClass({
    GTypeName: 'DemoWindow',
}, class extends Gtk.Window {
    _init(params) {
        // TODO: Report issue to GJS: Init params contains 3 keys for the same 'css-name' property.
        params = Object.assign({}, params);
        delete params.cssName;
        delete params.css_name;

        super._init(params);

        let actionGroup = new Gio.SimpleActionGroup();
        this.insert_action_group('win', actionGroup);

        [
            ['new', this._notImplemented.bind(this)],
            ['open', this._notImplemented.bind(this)],
            ['save', this._notImplemented.bind(this)],
            ['save-as', this._notImplemented.bind(this)],
            ['copy', this._notImplemented.bind(this)],
            ['cut', this._notImplemented.bind(this)],
            ['paste', this._notImplemented.bind(this)],
            ['quit', () => this.destroy()],
            ['about', () => this.aboutDialog.show()],
            ['help', () => this._statusMessage("Help not available")],
        ].forEach(action => {
            let [name, activate] = action;
            let simpleAction = new Gio.SimpleAction({ name });
            simpleAction.connect('activate', activate);
            actionGroup.add_action(simpleAction);
        });

        let controller = new Gtk.ShortcutController({ scope: Gtk.ShortcutScope.GLOBAL });
        this.add_controller(controller);

        [
            [Gdk.KEY_n, Gdk.ModifierType.CONTROL_MASK, 'win.new'],
            [Gdk.KEY_o, Gdk.ModifierType.CONTROL_MASK, 'win.open'],
            [Gdk.KEY_s, Gdk.ModifierType.CONTROL_MASK, 'win.save'],
            [Gdk.KEY_s, Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK, 'win.save-as'],
            [Gdk.KEY_c, Gdk.ModifierType.CONTROL_MASK, 'win.copy'],
            [Gdk.KEY_x, Gdk.ModifierType.CONTROL_MASK, 'win.cut'],
            [Gdk.KEY_v, Gdk.ModifierType.CONTROL_MASK, 'win.paste'],
            [Gdk.KEY_q, Gdk.ModifierType.CONTROL_MASK, 'win.quit'],
            [Gdk.KEY_F7, Gdk.ModifierType.CONTROL_MASK, 'win.about'],
            [Gdk.KEY_F1, Gdk.ModifierType.CONTROL_MASK, 'win.help'],
        ].forEach(shortcut => {
            let [keyval, modifiers, actionName] = shortcut;
            controller.add_shortcut(new Gtk.Shortcut({
                trigger: new Gtk.KeyvalTrigger({ keyval, modifiers }),
                action: Gtk.NamedAction.new(actionName)
            }));
        });
    }

    _statusMessage(text) {
        this.statusbar.push(0, text);

        GLib.timeout_add(GLib.PRIORITY_DEFAULT, 5000, () => {
            this.statusbar.pop(0);
            return GLib.SOURCE_REMOVE;
        });
    }

    _notImplemented(action) {
        this._statusMessage(`Action “${action.name}” not implemented`);
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_file(directory.get_child('demo.ui').get_path());
    let window = builder.get_object('window1');

    window.aboutDialog = builder.get_object('aboutdialog1');
    window.aboutDialog.set_transient_for(window);
    window.aboutDialog.set_hide_on_close(true);
    window.statusbar = builder.get_object('statusbar1');

    application.add_window(window);
    window.present();
});

application.run([]);
