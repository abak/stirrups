// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Application Class
 *
 * Demonstrates a simple application.
 *
 * This example uses GtkApplication, GtkApplicationWindow, GtkBuilder
 * as well as GMenu.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, GLib, GObject, Gtk } = imports.gi;
const ByteArray = imports.byteArray;

const directory = Gio.File.new_for_path(GLib.get_current_dir());

// GLib.setenv('GSETTINGS_SCHEMA_DIR', GLib.get_current_dir(), true);
const settings = Gio.Settings.new('com.example.GtkApplication');

const Window = GObject.registerClass({
    GTypeName: 'DemoApplicationWindow',
    Template: directory.get_child('application.ui').get_uri(),
    Children: [],
    InternalChildren: ['buffer', 'infobar', 'menubutton', 'message', 'status', 'toolmenu'],
}, class extends Gtk.ApplicationWindow {
    _init(params) {
        super._init(params);

        this._width = -1;
        this._height = -1;
        this._maximized = false;
        this._fullscreen = false;

        this._loadState();
        this.set_default_size(this._width, this._height);
        if (this._maximized)
            this.maximize();
        if (this._fullscreen)
            this.fullscreen();

        let popover = Gtk.PopoverMenu.new_from_model(this._toolmenu);
        this._menubutton.set_popover(popover);

        let actions = [
            {
                name: 'shape', activate: this._activateRadio,
                parameterType: GLib.VariantType.new('s'),
                state: GLib.Variant.new_string('ovale'),
                changeState: this._changeRadioSate
            }, {
                name: 'bold', activate: this._activateBold,
                state: GLib.Variant.new_boolean(false)
            },
            { name: 'about', activate: this._activateAbout },
            { name: 'file1', activate: this._activateAction },
            { name: 'logo', activate: this._activateAction },
        ];

        actions.forEach(action => {
            let simpleAction = new Gio.SimpleAction({
                name: action.name,
                state: action.state || null,
                parameterType: action.parameterType || null
            });

            simpleAction.connect('activate', action.activate.bind(this));
            if (action.changeState)
                simpleAction.connect('change-state', action.changeState.bind(this));
            this.add_action(simpleAction);
        });
    }

    _storeState() {
        settings.set_value('window-size', new GLib.Variant('(ii)', [this._width, this._height]));
        settings.set_boolean('maximized', this._maximized);
        settings.set_boolean('fullscreen', this._fullscreen);
    }

    _loadState() {
        [this._width, this._height] = settings.get_value('window-size').deepUnpack();
        this._maximized = settings.get_boolean('maximized');
        this._fullscreen = settings.get_boolean('fullscreen');
    }

    vfunc_size_allocate(width, height, baseline) {
        super.vfunc_size_allocate(width, height, baseline);

        if (!this.maximized && !this.fullscreen)
            [this._width, this._height] = this.get_default_size();
    }

    _onSurfaceStateChanged(surface) {
        let newState = surface.get_state();
        this._maximized = (newState & Gdk.ToplevelState.MAXIMIZED) != 0;
        this._fullscreen = (newState & Gdk.ToplevelState.FULLSCREEN) != 0;
    }

    vfunc_realize() {
        super.vfunc_realize();

        this._stateChangedHandler = this.get_surface().connect('notify::state', this._onSurfaceStateChanged.bind(this));
    }

    vfunc_unrealize() {
        if (this._stateChangedHandler)
            this.get_surface().disconnect(this._stateChangedHandler);

        super.vfunc_unrealize();
    }


    vfunc_close_request() {
        this._storeState();
        this.application = null;
        this.destroy();
    }

    // Template handler
    update_statusbar(buffer, window) {
        // clear any previous message, underflow is allowed
        this._status.pop(0);

        let count = buffer.get_char_count();
        let iter = buffer.get_iter_at_mark(buffer.get_insert());
        let row = iter.get_line();
        let column = iter.get_line_offset();

        let message = `Cursor at row ${row} column ${column} - ${count} chars in document`;
        this._status.push(0, message);
    }

    // Template handler
    mark_set_callback(buffer, newLocation_, mark_, window) {
        this.update_statusbar(buffer, window);
    }

    // Template handler
    clicked_cb() {
        this._infobar.hide();
    }

    loadFile(file) {
        try {
            let [success_, contents] = file.load_contents(null);
            this._buffer.set_text(ByteArray.toString(contents), -1);
        } catch(e) {
            let messageDialog = new Gtk.MessageDialog({
                transient_for: this,
                destroy_with_parent: true,
                message_type: Gtk.MessageType.ERROR,
                buttons: Gtk.ButtonsType.CLOSE,
                text: `Error loading file: ${e.message}`,
            });

            messageDialog.connect('response', () => messageDialog.destroy());
            messageDialog.show();
        }
    }

    toggleFullscreen() {
        this[this._fullscreen ? 'unfullscreen' : 'fullscreen']();
    }

    showActionDialog(action) {
        let dialog = new Gtk.MessageDialog({
            transient_for: this,
            destroy_with_parent: true,
            message_type: Gtk.MessageType.INFO,
            buttons: Gtk.ButtonsType.CLOSE,
            text: `You activated action: "${action.get_name()}"`,
        });

        dialog.connect('response', () => dialog.destroy());
        dialog.show();
    }

    _showActionInfobar(action, parameter) {
        this._message.set_text(
            `You activated radio action: "${action.name}".\n`
            + `Current value: ${parameter.get_string()[0]}`
        );

        this._infobar.show();
    }

    _changeRadioSate(action, state) {
        action.set_state(state);
    }

    _activateRadio(action, parameter) {
        this._showActionInfobar(action, parameter);
        action.change_state(parameter);
    }

    _activateBold(action, parameter_) {
        this.showActionDialog(action);
        action.change_state(GLib.Variant.new_boolean(!action.get_state().get_boolean()));
    }

    _activateAbout(action, parameter_) {
        new Gtk.AboutDialog({
            program_name: "GTK4 Demo Application",
            version: "1.0.0",
            copyright: "© 2020 The Authors",
            license_type: Gtk.License.GPL_3_0,
            website: "https://www.example.com",
            comments: "A GTK Application",
            artists: ["First designer https://example.com", "Second designer"],
            authors: ["First author <firstauthor@example.com>", "Second author"],
            documenters: ["<a href=\"https://example.com\">First documentor</a>", "Second documentor"],
            logo_icon_name: 'application-x-executable',
            title: "About GTK Demo Application",
            transient_for: this,
        }).show();
    }

    _activateAction(action, parameter_) {
        this.showActionDialog(action);
    }
});

const Application = GObject.registerClass(class Application extends Gtk.Application {
    _init() {
        super._init({
            // application_id: 'com.example.GtkApplication',
            flags: Gio.ApplicationFlags.HANDLES_OPEN,
        });

        let actions = [
            { name: 'new', activate: this._activateNew },
            { name: 'open', activate: this._activateOpen },
            { name: 'save', activate: this._activateSave },
            { name: 'save-as', activate: this._activateSave },
            { name: 'fullscreen', activate: this._activateFullscreen },
            { name: 'quit', activate: this._activateQuit },
            {
                name: 'dark', activate: this._activateDark,
                state: GLib.Variant.new_boolean(false),
                changeState: this._changeThemeState
            },
        ];

        actions.forEach(action => {
            let simpleAction = new Gio.SimpleAction({ name: action.name, state: action.state || null });
            simpleAction.connect('activate', action.activate.bind(this));
            if (action.changeState)
                simpleAction.connect('change-state', action.changeState.bind(this));
            this.add_action(simpleAction);
        });

        this.add_action(settings.create_action('color'));
    }

    _activateNew() {
        this.activate();
    }

    _activateOpen() {
        let native = Gtk.FileChooserNative.new('Open File', this.activeWindow, Gtk.FileChooserAction.OPEN, '_Open', '_Cancel');

        native.connect('response', (native, responseId) => {
            if (responseId == Gtk.ResponseType.ACCEPT)
                this.open([native.get_file()], "");

            native.unref();
            native.destroy();
        });

        native.show();

        // XXX: Prevent the dialog from being disposed.
        native.ref();
    }

    _activateSave(action, parameter_) {
        if (this.activeWindow)
            this.activeWindow.showActionDialog(action);
    }

    _activateFullscreen() {
        if (this.activeWindow)
            this.activeWindow.toggleFullscreen();
    }

    _activateQuit() {
        this.get_windows().forEach(window => window.close());
    }

    _activateDark(action, parameter_) {
        if (this.activeWindow)
            this.activeWindow.showActionDialog(action);
        action.change_state(GLib.Variant.new_boolean(!action.get_state().get_boolean()));
    }

    _changeThemeState(action_, state) {
        Gtk.Settings.get_default()['gtk-application-prefer-dark-theme'] = state.get_boolean();
    }

    vfunc_open(files, hint_) {
        this.activate();

        if (files[0])
            this.activeWindow.loadFile(files[0]);
    }

    vfunc_activate() {
        new Window({ application: this, show_menubar: true }).present();
    }

    vfunc_startup() {
        super.vfunc_startup();

        let builder = Gtk.Builder.new_from_file(directory.get_child('menus.ui').get_path());
        this.set_menubar(builder.get_object('menubar'));
    }
});

new Application().run([]);
