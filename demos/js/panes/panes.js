// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Paned Widgets
 *
 * The GtkPaned Widget divides its content area into two panes
 * with a divider in between that the user can adjust. A separate
 * child is placed into each pane. GtkPaned widgets can be split
 * horizontally or vertically. This test contains both a horizontal
 * and a vertical GtkPaned widget.
 *
 * There are a number of options that can be set for each pane.
 * You can use the Inspector to adjust the options for each side
 * of each widget.
 */

imports.gi.versions['Gtk'] = '4.0';
const Gtk = imports.gi.Gtk;

let application = new Gtk.Application();

application.connect('activate', () => {
    let labelParams = {
        marginStart: 4, marginEnd: 4,
        marginTop: 4, marginBottom: 4,
        hexpand: true, vexpand: true,
    };

    let hpaned = new Gtk.Paned({
        startChild: new Gtk.Label(Object.assign({ label: "Hi there" }, labelParams)),
        endChild: new Gtk.Label(Object.assign({ label: "Hello" }, labelParams)),
        shrinkStartChild: false,
        shrinkEndChild: false,
    });

    let vpaned = new Gtk.Paned({
        orientation: Gtk.Orientation.VERTICAL,
        startChild: hpaned,
        endChild: new Gtk.Label(Object.assign({ label: "Goodbye" }, labelParams)),
        shrinkStartChild: false,
        shrinkEndChild: false,
    });

    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        spacing: 8,
        marginStart: 8, marginEnd: 8,
        marginTop: 8, marginBottom: 8,
    });

    vbox.append(new Gtk.Frame({ child: vpaned }));

    new Gtk.Window({
        application,
        title: "Paned Widgets",
        defaultWidth: 330, defaultHeight: 250,
        resizable: false,
        child: vbox,
    }).present();
});

application.run([]);
