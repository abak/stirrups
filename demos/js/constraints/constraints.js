// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Simple Constraints
 *
 * GtkConstraintLayout provides a layout manager that uses relations
 * between widgets (also known as “constraints”) to compute the position
 * and size of each child.
 *
 * In addition to child widgets, the constraints can involve spacer
 * objects (also known as “guides”). This example has a guide between
 * the two buttons in the top row.
 *
 * Try resizing the window to see how the constraints react to update
 * the layout.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GObject, Gtk } = imports.gi;

/* Layout:
 *
 *   +-------------------------------------+
 *   | +-----------++-------++-----------+ |
 *   | |  Child 1  || Space ||  Child 2  | |
 *   | +-----------++-------++-----------+ |
 *   | +---------------------------------+ |
 *   | |             Child 3             | |
 *   | +---------------------------------+ |
 *   +-------------------------------------+
 *
 * Constraints:
 *
 *   super.start = child1.start - 8
 *   child1.width = child2.width
 *   child1.end = space.start
 *   space.end = child2.start
 *   child2.end = super.end - 8
 *   super.start = child3.start - 8
 *   child3.end = super.end - 8
 *   super.top = child1.top - 8
 *   super.top = child2.top - 8
 *   child1.bottom = child3.top - 12
 *   child2.bottom = child3.top - 12
 *   child3.height = child1.height
 *   child3.height = child2.height
 *   child3.bottom = super.bottom - 8
 *
 * To add some flexibility, we make the space
 * stretchable:
 *
 *   space.width >= 10
 *   space.width = 100
 *   space.width <= 200
 */

const SimpleGrid = GObject.registerClass(class SimpleGrid extends Gtk.Widget {
    _init(params) {
        super._init(params);

        this._children = [];

        this._guide = new Gtk.ConstraintGuide({
            name: 'space',
            minWidth:  10, minHeight: 10,
            natWidth: 100, natHeight: 10,
            maxWidth: 200, maxHeight: 20,
            strength: Gtk.ConstraintStrength.STRONG,
        });
        this.layoutManager.add_guide(this._guide);
    }

    // Cannot use vfunc_dispose() in GJS because it conflicts with the garbage collector.
    vfunc_unrealize() {
        super.vfunc_unrealize();

        this._children.forEach(child => child.unparent());
        delete this._children;
    }

    _addConstraintsForChild1() {
        let [child1] = this._children;
        let space = this._guide;

        [
            // child1.width <= 200
            new Gtk.Constraint({
                target: child1,
                targetAttribute: Gtk.ConstraintAttribute.WIDTH,
                relation: Gtk.ConstraintRelation.LE,
                constant: 200,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // super.start = child1.start - 8
            new Gtk.Constraint({
                target: null,
                targetAttribute: Gtk.ConstraintAttribute.START,
                relation: Gtk.ConstraintRelation.EQ,
                source: child1,
                sourceAttribute: Gtk.ConstraintAttribute.START,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // child1.end = space.start
            new Gtk.Constraint({
                target: child1,
                targetAttribute: Gtk.ConstraintAttribute.END,
                relation: Gtk.ConstraintRelation.EQ,
                source: space,
                sourceAttribute: Gtk.ConstraintAttribute.START,
                multiplier: 1,
                constant: 0,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // super.top = child1.top - 8
            new Gtk.Constraint({
                target: null,
                targetAttribute: Gtk.ConstraintAttribute.TOP,
                relation: Gtk.ConstraintRelation.EQ,
                source: child1,
                sourceAttribute: Gtk.ConstraintAttribute.TOP,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
        ].forEach(
            constraint => this.layoutManager.add_constraint(constraint)
        );
    }

    _addConstraintsForChild2() {
        let [child1, child2] = this._children;
        let space = this._guide;

        [
            // child1.width = child2.width
            new Gtk.Constraint({
                target: child1,
                targetAttribute: Gtk.ConstraintAttribute.WIDTH,
                relation: Gtk.ConstraintRelation.EQ,
                source: child2,
                sourceAttribute: Gtk.ConstraintAttribute.WIDTH,
                multiplier: 1,
                constant: 0,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // space.end = child2.start
            new Gtk.Constraint({
                target: space,
                targetAttribute: Gtk.ConstraintAttribute.END,
                relation: Gtk.ConstraintRelation.EQ,
                source: child2,
                sourceAttribute: Gtk.ConstraintAttribute.START,
                multiplier: 1,
                constant: 0,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // child2.end = super.end - 8
            new Gtk.Constraint({
                target: child2,
                targetAttribute: Gtk.ConstraintAttribute.END,
                relation: Gtk.ConstraintRelation.EQ,
                source: null,
                sourceAttribute: Gtk.ConstraintAttribute.END,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // super.top = child2.top - 8
            new Gtk.Constraint({
                target: null,
                targetAttribute: Gtk.ConstraintAttribute.TOP,
                relation: Gtk.ConstraintRelation.EQ,
                source: child2,
                sourceAttribute: Gtk.ConstraintAttribute.TOP,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
        ].forEach(
            constraint => this.layoutManager.add_constraint(constraint)
        );
    }

    _addConstraintsForChild3() {
        let [child1, child2, child3] = this._children;

        [
            // super.start = child3.start - 8
            new Gtk.Constraint({
                target: null,
                targetAttribute: Gtk.ConstraintAttribute.START,
                relation: Gtk.ConstraintRelation.EQ,
                source: child3,
                sourceAttribute: Gtk.ConstraintAttribute.START,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // child3.end = super.end - 8
            new Gtk.Constraint({
                target: child3,
                targetAttribute: Gtk.ConstraintAttribute.END,
                relation: Gtk.ConstraintRelation.EQ,
                source: null,
                sourceAttribute: Gtk.ConstraintAttribute.END,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // child1.bottom = child3.top - 12
            new Gtk.Constraint({
                target: child1,
                targetAttribute: Gtk.ConstraintAttribute.BOTTOM,
                relation: Gtk.ConstraintRelation.EQ,
                source: child3,
                sourceAttribute: Gtk.ConstraintAttribute.TOP,
                multiplier: 1,
                constant: -12,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // child2.bottom = child3.top - 12
            new Gtk.Constraint({
                target: child2,
                targetAttribute: Gtk.ConstraintAttribute.BOTTOM,
                relation: Gtk.ConstraintRelation.EQ,
                source: child3,
                sourceAttribute: Gtk.ConstraintAttribute.TOP,
                multiplier: 1,
                constant: -12,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // child3.height = child1.height
            new Gtk.Constraint({
                target: child3,
                targetAttribute: Gtk.ConstraintAttribute.HEIGHT,
                relation: Gtk.ConstraintRelation.EQ,
                source: child1,
                sourceAttribute: Gtk.ConstraintAttribute.HEIGHT,
                multiplier: 1,
                constant: 0,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // child3.height = child2.height
            new Gtk.Constraint({
                target: child3,
                targetAttribute: Gtk.ConstraintAttribute.HEIGHT,
                relation: Gtk.ConstraintRelation.EQ,
                source: child2,
                sourceAttribute: Gtk.ConstraintAttribute.HEIGHT,
                multiplier: 1,
                constant: 0,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
            // child3.bottom = super.bottom - 8
            new Gtk.Constraint({
                target: child3,
                targetAttribute: Gtk.ConstraintAttribute.BOTTOM,
                relation: Gtk.ConstraintRelation.EQ,
                source: null,
                sourceAttribute: Gtk.ConstraintAttribute.BOTTOM,
                multiplier: 1,
                constant: -8,
                strength: Gtk.ConstraintStrength.REQUIRED,
            }),
        ].forEach(
            constraint => this.layoutManager.add_constraint(constraint)
        );
    }

    add(widget) {
        if (this._children.length == 3) {
            log(`Cannot add ${widget}, the grid is full`);
            return;
        }

        widget.set_parent(this);
        this._children.push(widget);
        this[`_addConstraintsForChild${this._children.length}`]();
    }
});
SimpleGrid.set_layout_manager_type(Gtk.ConstraintLayout.$gtype);

let application = new Gtk.Application();

application.connect('activate', () => {
    let grid = new SimpleGrid({
        hexpand: true, vexpand: true,
    });
    grid.add(new Gtk.Button({ label: "Child 1" }));
    grid.add(new Gtk.Button({ label: "Child 2" }));
    grid.add(new Gtk.Button({ label: "Child 3" }));

    let box = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        spacing: 12,
    });
    box.append(grid);

    new Gtk.Window({
        application,
        title: "Simple Constraints",
        defaultWidth: 260,
        child: box,
    }).present();
});

application.run([]);
