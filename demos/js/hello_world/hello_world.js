// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-License-Identifier: GPL-3.0-or-later

/* Hello World
 *
 * gtk.org
 *
 */

imports.gi.versions['Gtk'] = '4.0';
const Gtk = imports.gi.Gtk;

// Create a new application
let app = new Gtk.Application();

// When the application is launched…
app.connect('activate', () => {
    // … create a new window …
    let win = new Gtk.ApplicationWindow({ application: app });
    // … with a button in it …
    let btn = new Gtk.Button({ label: 'Hello, World!' });
    // … which closes the window when clicked
    btn.connect('clicked', () => win.close());
    win.set_child(btn);
    win.present();
});

// Run the application
app.run([]);
