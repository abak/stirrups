// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Lists/File Browser
 *
 * This demo shows off the different layouts that are quickly achievable
 * with GtkListview and GtkGridView by implementing a file browser with
 * different views.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, GObject, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');
const home = Gio.File.parse_name('~');

// Register a class to provide a simple object that holds the data for the different views.
GObject.registerClass({
    GTypeName: 'FileBrowserView',
    Properties: {
        'factory': GObject.ParamSpec.object(
            'factory', "Factory", "The factory to use in the main view",
            GObject.ParamFlags.READWRITE, Gtk.BuilderListItemFactory.$gtype
        ),
        'icon-name': GObject.ParamSpec.string(
            'icon-name', "Icon name", "The icon to display for selecting this view",
            GObject.ParamFlags.READWRITE, null
        ),
        'orientation': GObject.ParamSpec.enum(
            'orientation', "Orientation", "The orientation of the view",
            GObject.ParamFlags.READWRITE, Gtk.Orientation.$gtype, Gtk.Orientation.VERTICAL
        ),
        'title': GObject.ParamSpec.string(
            'title', "Title", "The title to display for selecting this view",
            GObject.ParamFlags.READWRITE, null
        ),
    },
}, class extends GObject.Object {});

// The builder scope provides the signal handlers and the closures.
const BuilderJSScope = GObject.registerClass({
    Implements: [Gtk.BuilderScope],
}, class BuilderJSScope extends GObject.Object {
    vfunc_create_closure(builder, handlerName, flags, object) {
        return (...args) => this[handlerName](...args, object);
    }

    filebrowser_get_content_type(listItem_, fileInfo) {
        return fileInfo?.get_attribute_string('standard::content-type') ?? null;
    }

    filebrowser_get_display_name(listItem_, fileInfo) {
        return fileInfo?.get_attribute_string('standard::display-name') ?? null;
    }

    filebrowser_get_icon(listItem_, fileInfo) {
        return fileInfo?.get_attribute_object('standard::icon') ?? null;
    }

    filebrowser_get_size(listItem_, fileInfo) {
        return fileInfo ? GLib.format_size(fileInfo.get_attribute_uint64('standard::size')) : null;
    }

    filebrowser_up_clicked_cb(button_, directoryList) {
        let file = directoryList.file.get_parent();
        if (file)
            directoryList.file = file;
    }

    filebrowser_view_activated_cb(gridView_, position, directoryList) {
        let fileInfo = directoryList.get_item(position);
        if (fileInfo.get_file_type() == Gio.FileType.DIRECTORY)
            directoryList.file = fileInfo.get_attribute_object('standard::file');
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = new Gtk.Builder({ scope: new BuilderJSScope() });
    builder.add_from_file(directory.get_child('listview_filebrowser.ui').get_path());

    // Fill the model with the contents of the home directory.
    builder.get_object('dirlist').set_file(home);

    // Grab focus in the view.
    builder.get_object('view').grab_focus();

    let window = builder.get_object('window');
    window.set_application(application);
    window.present();

    let cssProvider = new Gtk.CssProvider();
    cssProvider.load_from_file(directory.get_child('listview_filebrowser.css'));
    Gtk.StyleContext.add_provider_for_display(window.display, cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
});

application.run([]);
