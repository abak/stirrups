// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Peg Solitaire
 *
 * This demo demonstrates how to use drag'n'drop to implement peg solitaire.
 *
 * Rules (from Wikipedia):
 *
 * “The objective is, making valid moves, to empty the entire board except for
 * a solitary peg in the central hole.”
 *
 * “A valid move is to jump a peg orthogonally over an adjacent peg into a hole
 * two positions away and then to remove the jumped peg.”
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, GLib, GObject, Graphene, Gtk } = imports.gi;
const GSound = imports.package.checkSymbol('GSound') ? imports.gi.GSound : null;

function playSound(eventId) {
    if (!Gtk.Settings.get_default().gtkEnableEventSounds)
        return;

    if (GSound) {
        try {
            GSound.Context.new(null).play_simple({
                [GSound.ATTR_EVENT_ID]: eventId,
            }, null);
        } catch(e) {
            logError(e);
        }

        return;
    }

    // Fallback if GSound is not installed.
    for (let datadir of GLib.get_system_data_dirs()) {
        let path = GLib.build_filenamev([
            datadir, 'sounds', 'freedesktop', 'stereo', `${eventId}.oga`,
        ]);

        if (GLib.file_test(path, GLib.FileTest.EXISTS)) {
            Gtk.MediaFile.new_for_filename(path).play();
            break;
        }
    }
}

function showDialog(window, success) {
    new Gtk.MessageDialog({
        transientFor: window, modal: true, destroyWithParent: true,
        text: success ? "You win" : "You loose",
        messageType: Gtk.MessageType.INFO,
        buttons: Gtk.ButtonsType.OK,
        visible: true,
    }).connect('response', dialog => dialog.destroy());
}

// A class for the pegs that get moved around in the game.
const PegPaintable = GObject.registerClass({
    Implements: [Gdk.Paintable],
}, class PegPaintable extends GObject.Object {
    // The flags are very useful to let GTK know that this image is never going to change.
    // This allows many optimizations and should therefore always be set.
    vfunc_get_flags() {
        return Gdk.PaintableFlags.CONTENTS | Gdk.PaintableFlags.SIZE;
    }

    vfunc_get_intrinsic_height() {
        return 32;
    }

    vfunc_get_intrinsic_width() {
        return 32;
    }

    // The snapshot function does the actual drawing of the paintable.
    vfunc_snapshot(snapshot, width, height) {
        snapshot.append_color(
            new Gdk.RGBA({ red: 0.6, green: 0.3, blue: 0, alpha: 1.0 }),
            new Graphene.Rect({ size: { width, height } })
        );
    }
});

// A class to manage DnD operations.
const PegImage = GObject.registerClass(class PegImage extends Gtk.Image {
    vfunc_constructed() {
        super.vfunc_constructed();

        // Set up the drag source. This is rather straightforward:
        // Set the supported actions (in our case, pegs can only be moved) and connect
        // all the relevant signals.
        // And because all drag'n'drop handling is done via event controllers, we need
        // to add the controller to the widget.
        let dragSource = new Gtk.DragSource({ actions: Gdk.DragAction.MOVE });
        dragSource.connect('prepare', this._prepare.bind(this));
        dragSource.connect('drag-begin', this._dragBegin.bind(this));
        dragSource.connect('drag-end', this._dragEnd.bind(this));
        this.add_controller(dragSource);

        // Set up the drop target. This is more involved, because the game logic goes here.
        // First we specify the data we accept (peg paintables) and we only want moves.
        // Then, like above, we connect our signals and add the controller to the widget.
        let dropTarget = Gtk.DropTarget.new(PegPaintable.$gtype, Gdk.DragAction.MOVE);
        dropTarget.connect('accept', this._accept.bind(this));
        dropTarget.connect('drop', this._drop.bind(this));
        this.add_controller(dropTarget);
    }

    // The user tries to start a drag operation. We check if the image contains
    // a peg paintable, and if so, we return the peg paintable as the content
    // to be dragged.
    _prepare(dragSource_, x_, y_) {
        if (!this.hasPeg)
            return null;

        let value = new GObject.Value();
        value.init(PegPaintable.$gtype);
        value.set_object(this.paintable);

        return Gdk.ContentProvider.new_for_value(value);
    }

    // This notifies us that the drag has begun. We can now set up the icon and
    // the widget for the ongoing drag.
    _dragBegin(dragSource, drag_) {
        // We guaranteed in the drag_prepare function above that we only start a
        // drag if a peg is available. So let's make sure we did not screw that up.
        if (!this.hasPeg)
            return;

        // We use the peg paintable as the drag icon.
        dragSource.set_icon(this.paintable, -2, -2);

        // We keep the peg paintable, so that we can get it back later if the drag fails.
        this._draggedPaintable = this.paintable;

        // Because we are busy dragging the peg, we want to unset it on the image.
        this.clear(); // <=> this.paintable = null
    }

    // This is called once a drag operation has ended (successfully or not).
    // We want to undo what we did in _dragBegin() above and react to a potential
    // move of the peg.
    _dragEnd(dragSource, drag_, deleteData) {
        // If the drag did not succeed, we need to undo what we did in _dragBegin()
        // and reinsert the peg paintable here.
        if (!deleteData) {
            playSound('bell');
            this.paintable = this._draggedPaintable;
        }

        delete this._draggedPaintable;
    }

    // Whenever a new drop operation starts, we need to check if we can accept it.
    // The default check unfortunately is not good enough, because it only checks
    // the data type. But we also need to check if our image can even accept data.
    _accept(dropTarget_, drop) {
        // First, check the drop is actually trying to drop a peg paintable.
        if (!drop.get_formats().contain_gtype(PegPaintable.$gtype))
            return false;

        // If the image already contains a peg, we cannot accept another one.
        if (this.hasPeg)
            return false;

        let value = new GObject.Value();
        value.init(PegPaintable.$gtype);

        try {
            if (!drop.drag.content.get_value(value))
                return false;
        } catch(e) {
            logError(e);
            return false;
        }

        let { x: sourceX, y: sourceY } = value.get_object();
        let [targetX, targetY] = this.parent.query_child(this);

        // If the peg was not moved 2 spaces horizontally or vertically, this was not
        // a valid jump. Reject it.
        if (!((Math.abs(targetX - sourceX) == 2 && targetY == sourceY)
            || (Math.abs(targetY - sourceY) == 2 && targetX == sourceX)))
            return false;

        // Get the widget that was jumped over. If it does not have a peg in it,
        // this move isn't valid.
        let jumped = this.parent.get_child_at((targetX + sourceX) / 2, (targetY + sourceY) / 2);
        if (!jumped.hasPeg)
            return false;

        // The drop operation is accepted.
        return true;
    }

    _drop(dropTarget_, valueContents, x_, y_) {
        if (!(valueContents instanceof PegPaintable))
            return false;

        let { x: sourceX, y: sourceY } = valueContents;
        let [targetX, targetY] = this.parent.query_child(this);

        // Clear the peg paintable of the jumped-over image.
        let jumped = this.parent.get_child_at((targetX + sourceX) / 2, (targetY + sourceY) / 2);
        jumped.clear(); // <=> jumped.paintable = null
        // Add the peg paintable to this and update its position data.
        this.paintable = Object.assign(valueContents, { x: targetX, y: targetY });

        // Maybe we have something to celebrate.
        this.parent.checkForEnd();

        // The drop operation is successful.
        return true;
    }

    get hasPeg() {
        return this.paintable instanceof PegPaintable;
    }
});

const PegGrid = GObject.registerClass(class PegGrid extends Gtk.Grid {
    static get styleProvider() {
        if (!this._styleProvider) {
            this._styleProvider = new Gtk.CssProvider();
            this._styleProvider.load_from_data('.solitaire-field { border: 1px solid lightgray; }');
        }

        return this._styleProvider;
    }

    _init() {
        super._init({
            halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER,
            rowSpacing: 6, columnSpacing: 6,
            rowHomogeneous: true, columnHomogeneous: true,
        });

        for (let x = 0; x < 7; x++)
            for (let y = 0; y < 7; y++) {
                if ((x < 2 || x > 4) && (y < 2 || y > 4))
                    continue;

                let image = new PegImage({
                    iconSize: Gtk.IconSize.LARGE,
                    // Attach the position to the peg paintable so we can access it during DnD operations.
                    paintable: (x != 3 || y != 3) ? Object.assign(new PegPaintable(), { x, y }) : null,
                });
                image.get_style_context().add_class('solitaire-field');
                image.get_style_context().add_provider(this.constructor.styleProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

                this.attach(image, x, y, 1, 1);
            }
    }

    // We have a peg at (x, y). Check if we can move the peg to (x + 2*dx, y + 2*dy).
    _checkMove(x, y, dx, dy) {
        let image = this.get_child_at(x + dx, y + dy);
        if (!image || !image.hasPeg)
            return 0;

        image = this.get_child_at(x + 2 * dx, y + 2 * dy);
        if (!image || image.hasPeg)
            return 0;

        return 1;
    }

    checkForEnd() {
        let [pegs, moves] = [0, 0];

        for (let x = 0; x < 7; x++)
            for (let y = 0; y < 7; y++) {
                let image = this.get_child_at(x, y);

                if (image?.hasPeg) {
                    pegs++;
                    moves += this._checkMove(x, y, 1, 0);
                    moves += this._checkMove(x, y, -1, 0);
                    moves += this._checkMove(x, y, 0, 1);
                    moves += this._checkMove(x, y, 0, -1);
                }

                if (pegs > 1 && moves > 0)
                    break;
            }

        if (pegs == 1 &&  this.get_child_at(3, 3)?.hasPeg) {
            showDialog(this.root, true);
            playSound('complete');
        } else if (moves == 0) {
            showDialog(this.root, false);
            playSound('dialog-error');
        }
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let window = new Gtk.Window({
        application, title: "Peg Solitaire",
        defaultWidth: 400, defaultHeight: 300,
        child: new PegGrid(),
    });

    let button = new Gtk.Button({ iconName: 'view-refresh-symbolic' });
    button.connect('clicked', () => {
        playSound('trash-empty');
        window.set_child(new PegGrid());
    });

    window.set_titlebar(new Gtk.HeaderBar());
    window.get_titlebar().pack_start(button);
    window.present();
});

application.run([]);
