// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* CSS Blend Modes
 *
 * You can blend multiple backgrounds using the CSS blend modes available.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, GObject, Gtk } = imports.gi;
const ByteArray = imports.byteArray;
const directory = Gio.File.new_for_path('.');
String.prototype.format = imports.format.format;

// Workaround to use a gresource without building.
let sourcePath = directory.get_child('css_blendmodes.gresource.xml').get_path();
let targetPath = Gio.File.new_tmp('XXXXXX.gresource')[0].get_path();
GLib.spawn_command_line_sync(`glib-compile-resources --target=${targetPath} ${sourcePath}`);
Gio.Resource.load(targetPath)._register();

let application = new Gtk.Application();

const BLEND_MODES = [
    { id: 'color', name: "Color" },
    { id: 'color-burn', name: "Color (burn)" },
    { id: 'color-dodge', name: "Color (dodge)" },
    { id: 'darken', name: "Darken" },
    { id: 'difference', name: "Difference" },
    { id: 'exclusion', name: "Exclusion" },
    { id: 'hard-light', name: "Hard Light" },
    { id: 'hue', name: "Hue" },
    { id: 'lighten', name: "Lighten" },
    { id: 'luminosity', name: "Luminosity" },
    { id: 'multiply', name: "Multiply" },
    { id: 'normal', name: "Normal" },
    { id: 'overlay', name: "Overlay" },
    { id: 'saturation', name: "Saturate" },
    { id: 'screen', name: "Screen" },
    { id: 'soft-light', name: "Soft Light" },
];

const ListBox = GObject.registerClass(class ListBox extends Gtk.ListBox {
    _init(cssProvider, params) {
        super._init(params);

        this._cssProvider = cssProvider;
        this.connect('row-activated', this._onRowActivated.bind(this));

        // Add a row for each blend mode available
        BLEND_MODES.forEach(blendMode => {
            let label = new Gtk.Label({ label: blendMode.name, xalign: 0 });
            let row = new Gtk.ListBoxRow({ child: label });
            this.insert(row, -1);
            row.blendModeId = blendMode.id;
            if (row.blendModeId == 'normal')
                this._normalRow = row;
        });

        this.select_row(this._normalRow);
    }

    vfunc_root() {
        super.vfunc_root();

        this._normalRow.emit('activate');
        this._normalRow.grab_focus();
    }

    _onRowActivated(listBox, row) {
        let contents = '';

        try {
            let bytes = Gio.resources_lookup_data('/css_blendmodes/css_blendmodes.css', Gio.ResourceLookupFlags.NONE);
            let byteArray = ByteArray.fromGBytes(bytes);
            contents = ByteArray.toString(byteArray);
        } catch(e) {
            logError(e);
        }

        let cssContents = contents.format(row.blendModeId, row.blendModeId, row.blendModeId);
        this._cssProvider.load_from_data(cssContents);
    }
});

application.connect('activate', () => {
    let builder = Gtk.Builder.new_from_resource('/css_blendmodes/blendmodes.ui');
    let window = builder.get_object('window');

    let provider = new Gtk.CssProvider();
    Gtk.StyleContext.add_provider_for_display(window.display, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    builder.get_object('scrolledwindow').child = new ListBox(provider, {});

    application.add_window(window);
    window.present();
});

application.run([]);
