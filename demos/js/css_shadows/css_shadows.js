// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Shadows
 *
 * This demo shows how to use CSS shadows.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, Gtk, Pango } = imports.gi;
const ByteArray = imports.byteArray;
const directory = Gio.File.new_for_path('.');

// Workaround to use a gresource without building.
let sourcePath = directory.get_child('css_shadows.gresource.xml').get_path();
let targetPath = Gio.File.new_tmp('XXXXXX.gresource')[0].get_path();
GLib.spawn_command_line_sync(`glib-compile-resources --target=${targetPath} ${sourcePath}`);
Gio.Resource.load(targetPath)._register();

let application = new Gtk.Application();

application.connect('activate', () => {
    let tagTable = new Gtk.TextTagTable();
    tagTable.add(new Gtk.TextTag({ name: 'warning', underline: Pango.Underline.SINGLE }));
    tagTable.add(new Gtk.TextTag({ name: 'error', underline: Pango.Underline.ERROR }));

    let buffer = new Gtk.TextBuffer({ tagTable });
    let provider = new Gtk.CssProvider();

    buffer.connect('changed', buffer => {
        buffer.remove_all_tags(buffer.get_start_iter(), buffer.get_end_iter());
        provider.load_from_data(buffer.text);
    });

    provider.connect('parsing-error', (provider, section, error) => {
        let startLocation = section.get_start_location();
        let endLocation = section.get_end_location();

        let startIter = buffer.get_iter_at_line_index(startLocation.lines, startLocation.line_bytes)[1];
        let endIter = buffer.get_iter_at_line_index(endLocation.lines, endLocation.line_bytes)[1];

        let tagName = GLib.quark_to_string(error.domain) == 'gtk-css-parser-warning-quark' ? 'warning' : 'error';
        buffer.apply_tag_by_name(tagName, startIter, endIter);
    });

    try {
        let bytes = Gio.resources_lookup_data('/css_shadows/css_shadows.css', Gio.ResourceLookupFlags.NONE);
        let byteArray = ByteArray.fromGBytes(bytes);
        let contents = ByteArray.toString(byteArray);
        buffer.set_text(contents, -1);
    } catch(e) {
        logError(e);
    }

    let textView = new Gtk.TextView({ buffer });
    let scrolledWindow = new Gtk.ScrolledWindow({ child: textView });

    let toolbar = new Gtk.Box({ spacing: 6, valign: Gtk.Align.CENTER });
    toolbar.append(new Gtk.Button({ iconName: 'go-next' }));
    toolbar.append(new Gtk.Button({ iconName: 'go-previous' }));
    toolbar.append(new Gtk.Button({ label: "Hello World" }));

    let paned = new Gtk.Paned({
        orientation: Gtk.Orientation.VERTICAL,
        startChild: toolbar, resizeStartChild: false,
        endChild: scrolledWindow,
    });

    let window = new Gtk.Window({
        application,
        title: "Shadows",
        defaultWidth: 400,
        defaultHeight: 300,
        child: paned,
    });

    Gtk.StyleContext.add_provider_for_display(window.display, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    window.present();
});

application.run([]);
