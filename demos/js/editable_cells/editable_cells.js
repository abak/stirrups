// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Editable Cells
 *
 * This demo demonstrates the use of editable cells in a GtkTreeView. If
 * you're new to the GtkTreeView widgets and associates, look into
 * the GtkListStore example first. It also shows how to use the
 * GtkCellRenderer::editing-started signal to do custom setup of the
 * editable widget.
 *
 * The cell renderers used in this demo are GtkCellRendererText,
 * GtkCellRendererCombo and GtkCellRendererProgress.
 *
 * Difference with GTK4 Demos: remove unused list store columns.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GObject, Gtk } = imports.gi;

const ItemColumn = { NUMBER: 0, PRODUCT: 1, YUMMY: 2 };
const NumberColumn = { TEXT: 0 };

const ARTICLES = [
    { number: 3, product: "bottles of coke", yummy: 20 },
    { number: 5, product: "packages of noodles", yummy: 50 },
    { number: 2, product: "packages of chocolate chip cookies", yummy: 90 },
    { number: 1, product: "can vanilla ice cream", yummy: 60 },
    { number: 6, product: "eggs", yummy: 10 },
];
const NEW_ARTICLE = { number: 0, product: "Description here", yummy: 50 };
const N_NUMBERS = 10;

function createItemsModel() {
    let store = Gtk.ListStore.new([GObject.TYPE_INT, GObject.TYPE_STRING, GObject.TYPE_INT]);

    for (let article of ARTICLES)
        store.set(store.append(), Object.values(ItemColumn), Object.values(article));

    return store;
}

function setNewItem(model, iter) {
    model.set(iter, Object.values(ItemColumn), Object.values(NEW_ARTICLE));
}

function createNumbersModel() {
    let store = Gtk.ListStore.new([GObject.TYPE_STRING]);

    for (let i = 0; i < N_NUMBERS; i++)
        store.set(store.append(), Object.values(NumberColumn), [String(i)]);

    return store;
}

let application = new Gtk.Application();

application.connect('activate', () => {
    let treeView = new Gtk.TreeView({
        vexpand: true,
        model: createItemsModel(),
    });
    treeView.get_selection().mode = Gtk.SelectionMode.SINGLE;

    // The number view column.
    {
        let renderer = new Gtk.CellRendererCombo({
            model: createNumbersModel(),
            textColumn: NumberColumn.TEXT,
            hasEntry: false, editable: true,
        });
        // TODO: Report issue to GTK. 5 is not displayed in the combo box.
        renderer.connect('editing-started', (renderer_, editable, pathString_) => {
            // editable is a Gtk.ComboBox.
            editable.set_row_separator_func((comboBoxModel, iter) => {
                return comboBoxModel.get_path(iter).get_indices()[0] == 5;
            });
        });
        renderer.connect('edited', (renderer_, pathString, newText) => {
            let iter = treeView.model.get_iter_from_string(pathString)[1];
            treeView.model.set(iter, [ItemColumn.NUMBER], [Number(newText)]);
        });
        let colummn = new Gtk.TreeViewColumn({ title: "Number" });
        colummn.pack_start(renderer, false);
        colummn.add_attribute(renderer, 'text', ItemColumn.NUMBER);
        treeView.append_column(colummn);
    }

    // The product view column.
    {
        let renderer = new Gtk.CellRendererText({ editable: true });
        renderer.connect('edited', (renderer_, pathString, newText) => {
            let iter = treeView.model.get_iter_from_string(pathString)[1];
            treeView.model.set(iter, [ItemColumn.PRODUCT], [newText]);
        });
        let colummn = new Gtk.TreeViewColumn({ title: "Product" });
        colummn.pack_start(renderer, false);
        colummn.add_attribute(renderer, 'text', ItemColumn.PRODUCT);
        treeView.append_column(colummn);
    }

    // The yummy view column
    {
        let renderer = new Gtk.CellRendererProgress();
        let colummn = new Gtk.TreeViewColumn({ title: "Yummy" });
        colummn.pack_start(renderer, false);
        colummn.add_attribute(renderer, 'value', ItemColumn.YUMMY);
        treeView.append_column(colummn);
    }

    // A controller to add an item.
    let addButton = Gtk.Button.new_with_label("Add item");
    addButton.connect('clicked', () => {
        // Insert a new row below the current one.
        let [path, column_] = treeView.get_cursor();
        let iter = path ?
            treeView.model.insert_after(treeView.model.get_iter(path)[1]) :
            treeView.model.append();

        // Set the data for the new row.
        setNewItem(treeView.model, iter);

        // Move focus to the new row.
        let newPath = treeView.model.get_path(iter);
        treeView.set_cursor(newPath, treeView.get_column(0), false);
    });

    // A controller to remove an item.
    let removeButton = Gtk.Button.new_with_label("Remove item");
    removeButton.connect('clicked', () => {
        let iter = treeView.get_selection().get_selected()[2];
        if (iter)
            treeView.model.remove(iter);
    });

    let hbox = new Gtk.Box({
        homogeneous: true,
        spacing: 4,
    });
    hbox.append(addButton);
    hbox.append(removeButton);

    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        marginTop: 5, marginBottom: 5,
        marginStart: 5, marginEnd: 5,
        spacing: 5,
    });
    vbox.append(Gtk.Label.new("Shopping list (you can edit the cells!)"));
    vbox.append(new Gtk.ScrolledWindow({
        hasFrame: true,
        child: treeView,
    }));
    vbox.append(hbox);

    new Gtk.Window({
        application, title: "Editable Cells",
        defaultWidth: 320, defaultHeight: 200,
        child: vbox,
    }).present();
});

application.run([]);
