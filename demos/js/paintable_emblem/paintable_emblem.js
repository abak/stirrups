// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Emblems
 *
 * This demo shows how GdkPaintable can be used to
 * overlay an emblem on top of an icon. The emblem
 * can either be another icon, or an arbitrary
 * paintable.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gtk: '4.0' });
const { Gdk, GLib, GObject, Graphene, Gtk } = imports.gi;

const MAX_PROGRESS = 500;
const RADIUS = 0.3;

// See the "Animated paintable" demo for explanations.
const NuclearAnimationWithoutBackground = GObject.registerClass({
    Implements: [Gdk.Paintable],
}, class NuclearAnimationWithoutBackground extends GObject.Object {
    _init(animate = true, progress = 0) {
        super._init();

        this._progress = progress;
        if (!(this._animated = animate))
            return;

        GLib.timeout_add(GLib.PRIORITY_DEFAULT, 10, () => {
            this._progress = (this._progress + 1) % MAX_PROGRESS;
            this.invalidate_contents();

            return GLib.SOURCE_CONTINUE;
        });
    }

    vfunc_get_current_image() {
        return this._animated ? new this.constructor(false, this._progress) : this;
    }

    vfunc_get_flags() {
        return this._animated ? Gdk.PaintableFlags.SIZE :
            Gdk.PaintableFlags.CONTENTS | Gdk.PaintableFlags.SIZE;
    }

    // The same function as the "Animated Paintable" demo one, minus the background.
    vfunc_snapshot(snapshot, width, height) {
        let min = Math.min(width, height);
        let cr = snapshot.append_cairo(
            new Graphene.Rect().init((width - min) / 2, (height - min) / 2, min, min)
        );

        cr.translate(width / 2, height / 2);
        cr.scale(min, min);
        cr.rotate(2 * Math.PI * this._progress / MAX_PROGRESS);

        cr.arc(0, 0, 0.1, -Math.PI, Math.PI);
        cr.fill();

        cr.setLineWidth(RADIUS);
        cr.setDash([RADIUS * Math.PI / 3], 0);
        cr.arc(0, 0, RADIUS, -Math.PI, Math.PI);
        cr.stroke();

        cr.$dispose();
    }
});

// Returns a Gtk.IconPaintable instance, that is a paintable.
const getIconPaintable = function(iconName) {
    let theme = Gtk.IconTheme.get_for_display(Gdk.Display.get_default());
    return theme.lookup_icon(iconName, null, 128, 1, Gtk.TextDirection.LTR, 0);
};

const EmblemedIcon = GObject.registerClass({
    Implements: [Gdk.Paintable],
    Properties: {
        'emblem-paintable': GObject.ParamSpec.object(
            'emblem-paintable', "Emblem paintable", "The paintable the emblem is drawn with",
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, Gdk.Paintable.$gtype
        ),
        'icon-paintable': GObject.ParamSpec.object(
            'icon-paintable', "Icon paintable", "The paintable the icon is drawn with",
            GObject.ParamFlags.READWRITE, Gtk.IconPaintable.$gtype
        ),
    },
}, class EmblemedIcon extends GObject.Object {
    static newForIconNames(iconName, emblemIconName) {
        return new this({
            emblemPaintable: getIconPaintable(emblemIconName),
            iconPaintable: emblemIconName ? getIconPaintable(iconName) : null,
        });
    }

    get emblemPaintable() {
        return this._emblemPaintable ?? null;
    }

    set emblemPaintable(emblemPaintable) {
        if (this.emblemPaintable == emblemPaintable)
            return;

        if (this.emblemPaintable)
            this.emblemPaintable.disconnect(this._emblemHandler);

        this._emblemPaintable = emblemPaintable;
        this.notify('emblem-paintable');

        // Chain the 'invalidate-contents' signal.
        this._emblemHandler = emblemPaintable?.connect('invalidate-contents', () => {
            this.invalidate_contents();
        });
    }

    // A clone with a static emblem paintable.
    vfunc_get_current_image() {
        if (this.get_flags() & Gdk.PaintableFlags.CONTENTS)
            return this;
        else
            return new this.constructor({
                emblemPaintable: this.emblemPaintable.get_current_image(),
                iconPaintable: this.iconPaintable,
            });
    }

    // Bind the static contents flag with the emblem one (the icon paintable is always static).
    vfunc_get_flags() {
        return Gdk.PaintableFlags.SIZE | (this.emblemPaintable?.get_flags() ?? Gdk.PaintableFlags.CONTENTS);
    }

    vfunc_snapshot(snapshot, width, height) {
        // Draw the icon.
        this.iconPaintable.snapshot(snapshot, width, height);

        if (!this.emblemPaintable)
            return;

        // Draw the emblem.
        snapshot.save();
        snapshot.translate(new Graphene.Point({ x: width / 2 }));
        this.emblemPaintable.snapshot(snapshot, width / 2, height / 2);
        snapshot.restore();
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let grid = new Gtk.Grid();

    grid.attach(new Gtk.Image({
        hexpand: true, vexpand: true,
        paintable: EmblemedIcon.newForIconNames('folder', 'starred'),
    }), 0, 0, 1, 1);

    grid.attach(new Gtk.Image({
        hexpand: true, vexpand: true,
        paintable: new EmblemedIcon({
            emblemPaintable: new NuclearAnimationWithoutBackground(),
            iconPaintable: getIconPaintable('drive-multidisk'),
        }),
    }), 1, 0, 1, 1);

    new Gtk.Window({
        application, title: "Paintable — Emblems",
        defaultWidth: 300, defaultHeight: 200,
        child: grid,
    }).present();
});

application.run([]);
