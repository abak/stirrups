// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Completion
 *
 * GtkEntryCompletion provides a mechanism for adding support for
 * completion in GtkEntry.
 */

imports.gi.versions['Gtk'] = '4.0';
const { GObject, Gtk } = imports.gi;

const strings = [
    "GNOME", "gnominious", "Gnomonic projection", "Gnosophy",
    "total", "totally", "toto", "tottery", "totterer", "Totten trust",
    "Tottenham hotspurs", "totipotent", "totipotency", "totemism",
    "totem pole", "Totara", "totalizer", "totalizator", "totalitarianism",
    "total parenteral nutrition", "total eclipse", "Totipresence",
    "Totipalmi", "zombie", "aæx", "aæy", "aæz",
];

let application = new Gtk.Application();

application.connect('activate', () => {
    // GtkListStore implements GtkTreeModel, as required.
    let store = Gtk.ListStore.new([GObject.TYPE_STRING]);
    strings.forEach(string => store.insert_with_values(-1, [0], [string]));

    let completion = new Gtk.EntryCompletion({
        inlineCompletion: true, inlineSelection: true,
        model: store,
    });
    // ⚠️ It is more than a simple 'text-column' setter. From the GTK documentation:
    // "This functions creates and adds a GtkCellRendererText for the selected column."
    // Without using this function, the completion popup cannot display the strings.
    completion.set_text_column(0);

    let window = new Gtk.Window({
        application, title: "Completion",
        resizable: false,
        child: new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            marginTop: 18, marginBottom: 18,
            marginStart: 18, marginEnd: 18,
            spacing: 12,
        }),
    });

    window.child.append(new Gtk.Label({
        label: "Try writing <b>total</b> or <b>gnome</b> for example.",
        useMarkup: true,
    }));
    window.child.append(new Gtk.Entry({ completion }));

    window.present();
});

application.run([]);
