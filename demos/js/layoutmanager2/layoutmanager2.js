// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Layout Manager/Transformation
 *
 * This demo shows how to use transforms in a nontrivial
 * way with a custom layout manager. The layout manager places
 * icons on a sphere that can be rotated using arrow keys. Press
 * also the Shift key to use Tweener animations.
 *
 * Note: It seems that JavaScript is not the best place to
 * compute matrices. In a real application, the decomposition
 * functions would stay in a C library.
 */

Object.assign(imports.gi.versions, { Gdk: '4.0', Gsk: '4.0', Gtk: '4.0' });
const { Gdk, Gio, GLib, GObject, Gtk } = imports.gi;
const System = imports.system;
const Tweener = imports.tweener.tweener;
void imports.demo2layout;

// XXX: Workaround for the "offending callback" GJS critical error.
Gtk.Widget.prototype.addTickCallbackOld = Gtk.Widget.prototype.add_tick_callback;
Gtk.Widget.prototype.add_tick_callback = function(callback) {
    System.gc();
    let time = Date.now();

    return this.addTickCallbackOld(function(...args) {
        // Anticipate the next garbage collection to prevent tick
        // callbacks from "appearing" during a garbage collection.
        if ((Date.now() - time) >= 3000) {
            System.gc();
            time = Date.now();
        }

        return callback(...args);
    });
};

const DURATION = 500; // Transition duration, in ms.

const DemoWidget = GObject.registerClass(class DemoWidget extends Gtk.Widget {
    _init(params) {
        super._init(Object.assign({
            focusable: true,
        }, params));

        this._layoutManager = this.layoutManager;

        let action, actionGroup;
        this.insert_action_group('move', actionGroup = new Gio.SimpleActionGroup());
        actionGroup.add_action(action = new Gio.SimpleAction({
            name: 'rotate', parameterType: GLib.VariantType.new('(iib)'),
        }));
        action.connect('activate', (action_, parameter) => {
            let [orientation, direction, tweener] = parameter.deepUnpack();

            if (tweener)
                this._rotateTweener(orientation, direction);
            else
                this._rotateTick(orientation, direction);
        });

        let shortcutController = new Gtk.ShortcutController({
            scope: Gtk.ShortcutScope.LOCAL,
        });
        [
            [Gdk.KEY_Left, [Gtk.Orientation.HORIZONTAL, -1]],
            [Gdk.KEY_Right, [Gtk.Orientation.HORIZONTAL, 1]],
            [Gdk.KEY_Up, [Gtk.Orientation.VERTICAL, 1]],
            [Gdk.KEY_Down, [Gtk.Orientation.VERTICAL, -1]],
        ].forEach(([keyval, tupple]) => {
            shortcutController.add_shortcut(Gtk.Shortcut.new(
                Gtk.KeyvalTrigger.new(keyval, 0),
                Gtk.CallbackAction.new(widget => widget.activate_action(
                    'move.rotate',
                    GLib.Variant.new('(iib)', tupple.concat([false]))
                ))
            ));
            shortcutController.add_shortcut(Gtk.Shortcut.new(
                Gtk.KeyvalTrigger.new(keyval, Gdk.ModifierType.SHIFT_MASK),
                Gtk.CallbackAction.new(widget => widget.activate_action(
                    'move.rotate',
                    GLib.Variant.new('(iib)', tupple.concat([true]))
                ))
            ));
        });
        this.add_controller(shortcutController);
    }

    _rotateTick(orientation, direction) {
        this._endPosition = this._startPosition = this._layoutManager.position;
        this._endOffset = this._startOffset = this._layoutManager.offset;
        this[orientation == Gtk.Orientation.HORIZONTAL ? '_endPosition' : '_endOffset'] += 10 * direction;

        this._startTime = this.get_frame_clock().get_frame_time();
        this._endTime = this._startTime + DURATION * GLib.TIME_SPAN_MILLISECOND;

        if (this._animating)
            return;

        this._animating = true;
        this.add_tick_callback((widget_, frameClock) => {
            let now = frameClock.get_frame_time();
            if (now >= this._endTime) {
                this._animating = false;
                return GLib.SOURCE_REMOVE;
            }

            let t = (now - this._startTime) / (this._endTime - this._startTime);
            // ease_out_cubic
            t = (t - 1) ** 3 + 1;

            this._layoutManager.position = Math.round(this._startPosition + t * (this._endPosition - this._startPosition));
            this._layoutManager.offset = Math.round(this._startOffset + t * (this._endOffset - this._startOffset));

            return GLib.SOURCE_CONTINUE;
        });
    }

    _rotateTweener(orientation, direction) {
        let now = Date.now();
        if (now - (this._gcTime ?? 0) >= 3000) {
            this._gcTime = now;
            System.gc();
        }

        let key = orientation == Gtk.Orientation.HORIZONTAL ? 'position' : 'offset';
        let value = this._layoutManager[key] + 10 * direction;

        Tweener.addTween(this._layoutManager, {
            [key]: value,
            time: DURATION / 1000,
            transition: 'easeOutCubic',
            rounded: true,
        });
    }

    vfunc_snapshot(snapshot) {
        [...this].forEach(child => {
            // Our layout manager sets this for children that are out of view.
            if (!child.get_child_visible())
                return;

            this.snapshot_child(child, snapshot);
        });
    }

    vfunc_unrealize() {
        super.vfunc_unrealize();

        [...this].forEach(child => child.unparent());
    }

    addChild(child) {
        child.set_parent(this);
    }
});

// Here is where we use our custom layout manager.
DemoWidget.set_layout_manager_type(GObject.type_from_name('Demo2Layout'));

const iconNames = [
    "action-unavailable-symbolic",
    "address-book-new-symbolic",
    "application-exit-symbolic",
    "appointment-new-symbolic",
    "bookmark-new-symbolic",
    "call-start-symbolic",
    "call-stop-symbolic",
    "camera-switch-symbolic",
    "chat-message-new-symbolic",
    "color-select-symbolic",
    "contact-new-symbolic",
    "document-edit-symbolic",
    "document-new-symbolic",
    "document-open-recent-symbolic",
    "document-open-symbolic",
    "document-page-setup-symbolic",
    "document-print-preview-symbolic",
    "document-print-symbolic",
    "document-properties-symbolic",
    "document-revert-symbolic-rtl",
    "document-revert-symbolic",
    "document-save-as-symbolic",
    "document-save-symbolic",
    "document-send-symbolic",
    "edit-clear-all-symbolic",
    "edit-clear-symbolic-rtl",
    "edit-clear-symbolic",
    "edit-copy-symbolic",
    "edit-cut-symbolic",
    "edit-delete-symbolic",
    "edit-find-replace-symbolic",
    "edit-find-symbolic",
    "edit-paste-symbolic",
    "edit-redo-symbolic-rtl",
    "edit-redo-symbolic",
    "edit-select-all-symbolic",
    "edit-select-symbolic",
    "edit-undo-symbolic-rtl",
    "edit-undo-symbolic",
    "error-correct-symbolic",
    "find-location-symbolic",
    "folder-new-symbolic",
    "font-select-symbolic",
    "format-indent-less-symbolic-rtl",
    "format-indent-less-symbolic",
    "format-indent-more-symbolic-rtl",
    "format-indent-more-symbolic",
    "format-justify-center-symbolic",
    "format-justify-fill-symbolic",
    "format-justify-left-symbolic",
    "format-justify-right-symbolic",
    "format-text-bold-symbolic",
    "format-text-direction-symbolic-rtl",
    "format-text-direction-symbolic",
    "format-text-italic-symbolic",
    "format-text-strikethrough-symbolic",
    "format-text-underline-symbolic",
    "go-bottom-symbolic",
    "go-down-symbolic",
    "go-first-symbolic-rtl",
    "go-first-symbolic",
    "go-home-symbolic",
    "go-jump-symbolic-rtl",
    "go-jump-symbolic",
    "go-last-symbolic-rtl",
    "go-last-symbolic",
    "go-next-symbolic-rtl",
    "go-next-symbolic",
    "go-previous-symbolic-rtl",
    "go-previous-symbolic",
    "go-top-symbolic",
    "go-up-symbolic",
    "help-about-symbolic",
    "insert-image-symbolic",
    "insert-link-symbolic",
    "insert-object-symbolic",
    "insert-text-symbolic",
    "list-add-symbolic",
    "list-remove-all-symbolic",
    "list-remove-symbolic",
    "mail-forward-symbolic",
    "mail-mark-important-symbolic",
    "mail-mark-junk-symbolic",
    "mail-mark-notjunk-symbolic",
    "mail-message-new-symbolic",
    "mail-reply-all-symbolic",
    "mail-reply-sender-symbolic",
    "mail-send-receive-symbolic",
    "mail-send-symbolic",
    "mark-location-symbolic",
    "media-eject-symbolic",
    "media-playback-pause-symbolic",
    "media-playback-start-symbolic",
    "media-playback-stop-symbolic",
    "media-record-symbolic",
    "media-seek-backward-symbolic",
    "media-seek-forward-symbolic",
    "media-skip-backward-symbolic",
    "media-skip-forward-symbolic",
    "media-view-subtitles-symbolic",
    "object-flip-horizontal-symbolic",
    "object-flip-vertical-symbolic",
    "object-rotate-left-symbolic",
    "object-rotate-right-symbolic",
    "object-select-symbolic",
    "open-menu-symbolic",
    "process-stop-symbolic",
    "send-to-symbolic",
    "sidebar-hide-symbolic",
    "sidebar-show-symbolic",
    "star-new-symbolic",
    "system-log-out-symbolic",
    "system-reboot-symbolic",
    "system-run-symbolic",
    "system-search-symbolic",
    "system-shutdown-symbolic",
    "system-switch-user-symbolic",
    "tab-new-symbolic",
    "tools-check-spelling-symbolic",
    "value-decrease-symbolic",
    "value-increase-symbolic",
    "view-app-grid-symbolic",
    "view-conceal-symbolic",
    "view-continuous-symbolic",
    "view-dual-symbolic",
    "view-fullscreen-symbolic",
    "view-grid-symbolic",
    "view-list-bullet-symbolic",
    "view-list-ordered-symbolic",
    "view-list-symbolic",
    "view-mirror-symbolic",
    "view-more-horizontal-symbolic",
    "view-more-symbolic",
    "view-paged-symbolic",
    "view-pin-symbolic",
    "view-refresh-symbolic",
    "view-restore-symbolic",
    "view-reveal-symbolic",
    "view-sort-ascending-symbolic",
    "view-sort-descending-symbolic",
    "zoom-fit-best-symbolic",
    "zoom-in-symbolic",
    "zoom-original-symbolic",
    "zoom-out-symbolic",
];

let application = new Gtk.Application();

application.connect('activate', () => {
    let widget = new DemoWidget();

    for (let i = 0; i < 18 * 36; i++) {
        widget.addChild(new Gtk.Image({
            iconName: iconNames[i % iconNames.length],
            marginStart: 4, marginEnd: 4,
            marginTop: 4, marginBottom: 4,
        }));
    }

    new Gtk.Window({
        application, title: "Layout Manager — Transformation",
        defaultWidth: 600, defaultHeight: 620,
        child: widget,
    }).present();
});

application.run([]);
