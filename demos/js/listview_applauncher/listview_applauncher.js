// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Application Launcher
 *
 * This demo uses the GtkListView widget as a fancy application launcher.
 *
 * It is also a very small introduction to listviews.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, Gtk } = imports.gi;

// Prepare the context for launching the application and launch it.
// See the documentation for GdkAppLaunchContext and GAppInfo.
function launch(appInfo, window) {
    let context = window.display.get_app_launch_context();

    try {
        appInfo.launch([], context);
    } catch(e) {
        new Gtk.MessageDialog({
            transientFor: window, modal: true, destroyWithParent: true,
            messageType: Gtk.MessageType.ERROR,
            buttons: Gtk.ButtonsType.CLOSE,
            text: `Could not launch “${appInfo.get_display_name()}”`,
            secondaryText: e.message,
            visible: true,
        }).connect('response', dialog => dialog.destroy());
    }
}

// GTK list widgets need a GListModel to display, as models support change notifications.
// We use a GListStore here, which is a simple array-like list implementation for manual management.
// As we want to do a list of applications, GAppInfo is the type we provide.
let store = new Gio.ListStore({ itemType: Gio.AppInfo.$gtype });
Gio.AppInfo.get_all().forEach(appInfo => store.append(appInfo));

// The GtkListitemFactory is what is used to create GtkListItems to display the data from the model.
// So it is absolutely necessary to create one. We will use a GtkSignalListItemFactory because it is
// the simplest one to use.
let factory = new Gtk.SignalListItemFactory();

// This is the callback we use for setting up new listitems to display. We add just an image and
// a label here to display the application's icon and name.
factory.connect('setup', (factory_, listItem) => {
    listItem.child = new Gtk.Box({ spacing: 12 });
    listItem.child.append(new Gtk.Image({ iconSize: Gtk.IconSize.LARGE }));
    listItem.child.append(new Gtk.Label());
});

// Here we need to prepare the listitem for displaying its item. We get the listitem already set up
// from the previous callback, so we can reuse the widgets we set up above. We get the item
// (which we know is a GAppInfo because it comes out of the model we set up above), grab its icon
// and display it.
factory.connect('bind', (factory_, listItem) => {
    let appInfo = listItem.item;
    listItem.child.get_first_child().gicon = appInfo.get_icon();
    listItem.child.get_last_child().label = appInfo.get_display_name();
});

// In more complex code, we would also need callbacks to unbind and teardown the listitem. If we had
// connected signals, this step would have been necessary.

let application = new Gtk.Application();

application.connect('activate', () => {
    // The list will take items from the model and use the factory to create as many listitems as it
    // needs to show itself to the user.
    // Note that the store is wrapped by a GtkSelectionModel.
    let listView = new Gtk.ListView({
        model: new Gtk.SingleSelection({ model: store }),
        factory,
    });

    // This signal is emitted whenever an item in the list is activated. This is the simple way to
    // allow reacting to the Enter key or double-clicking on a listitem. Of course, it is possible to
    // use far more complex interactions by turning off activation and adding buttons or other widgets
    // in the factory setup callback.
    listView.connect('activate', (listView, position) => {
        let appInfo = listView.model.get_item(position);
        launch(appInfo, listView.root);
    });

    // List widgets should always be contained in a GtkScrolledWindow, because otherwise they might
    // get too large or they might not be scrollable.
    let scrolledWindow = new Gtk.ScrolledWindow({
        child: listView,
    });

    new Gtk.Window({
        application, title: "Application Launcher",
        defaultWidth: 640, defaultHeight: 320,
        child: scrolledWindow,
    }).present();
});

application.run([]);
