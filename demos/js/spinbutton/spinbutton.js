// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Spin Buttons
 *
 * GtkSpinButton provides convenient ways to input data
 * that can be seen as a value in a range. The examples
 * here show that this does not necessarily mean numeric
 * values, and it can include custom formatting.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, GObject, Gtk } = imports.gi;
const directory = Gio.File.new_for_path('.');
const vprintf = imports.format.vprintf;

const MONTHS = [
    "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December",
];

const callbacks = {
    spinbutton_hex_spin_input: function(spinButton, newVal_) {
        let [integer, error] = GLib.ascii_strtoll(spinButton.text, 16);

        if (error) {
            spinButton.adjustment.value = 0;
            return Gtk.INPUT_ERROR;
        }

        // Cannot return newVal.
        spinButton.adjustment.value = integer;
        return false;
    },

    spinbutton_hex_spin_output: function(spinButton) {
        let value = spinButton.adjustment.value;
        let string = parseInt(value).toString(16).toUpperCase();
        let text = (string.length == 1 ? '0x0' : '0x') + string;

        if (spinButton.text != text)
            spinButton.text = text;

        return true;
    },

    spinbutton_time_spin_input: function(spinButton, newVal_) {
        let [hours, minutes] = spinButton.text.split(':').map(s => parseInt(s));

        // Cannot return newVal.
        spinButton.adjustment.value = 60 * hours + minutes;
        return false;
    },

    spinbutton_time_spin_output: function(spinButton) {
        let value = spinButton.adjustment.value;
        let [hours, minutes] = [value / 60, value % 60];
        let text = vprintf("%02.0f:%02.0f", [Math.floor(hours), Math.round(minutes)]);

        if (spinButton.text != text)
            spinButton.text = text;

        return true;
    },

    spinbutton_month_spin_input: function(spinButton, newVal_) {
        let text = spinButton.text.toLowerCase();

        for (let i = 0; i < MONTHS.length; i++) {
            if (MONTHS[i].toLowerCase().startsWith(text)) {
                // Cannot return newVal.
                spinButton.adjustment.value = i + 1;
                return false;
            }
        }

        spinButton.adjustment.value = 1;
        return Gtk.INPUT_ERROR;
    },

    spinbutton_month_spin_output: function(spinButton) {
        let value = spinButton.adjustment.value;
        let text = MONTHS[value - 1];

        if (spinButton.text != text)
            spinButton.text = text;

        return true;
    },
};

// GJS handles the "scope" machinery for widget class templates, whose an example is
// provided by the "Application Class" demo. Here is a generic builder scope to bind
// signal handlers manually. Contrary to the GJS one, it accepts swapped handlers.
const BuilderJSScope = GObject.registerClass({
    GTypeName: 'BuilderJSScope',
    Implements: [Gtk.BuilderScope],
}, class extends GObject.Object {
    vfunc_create_closure(builder, handlerName, flags, object) {
        const callback = callbacks[handlerName];
        const swapped = flags & Gtk.BuilderClosureFlags.SWAPPED;

        return swapped ?
            (...args) => callback(object, ...args.slice(1), args[0]) :
            (...args) => callback(...args, object);
    }
});

let application = new Gtk.Application();

application.connect('activate', () => {
    let builder = new Gtk.Builder({ scope: new BuilderJSScope() });
    builder.add_from_file(directory.get_child('spinbutton.ui').get_path());
    let window = builder.get_object('window');

    [
        ['basic_adjustment', 'basic_label'],
        ['hex_adjustment', 'hex_label'],
        ['time_adjustment', 'time_label'],
        ['month_adjustment', 'month_label'],
    ].forEach(([adjustmentName, labelName]) => {
        let adjustment = builder.get_object(adjustmentName);
        let label = builder.get_object(labelName);
        adjustment.connect('notify::value', () => {
            label.label = String(adjustment.value);
        });
        adjustment.notify('value');
    });

    // TODO: Report bug to GJS.
    // Get "JS ERROR: Error: Unknown signal" with the "input" signal handlers from the builder file.
    [
        ['hex_spin', 'spinbutton_hex_spin_input'],
        ['time_spin', 'spinbutton_time_spin_input'],
        ['month_spin', 'spinbutton_month_spin_input'],
    ].forEach(([objectName, handlerName]) => {
        let spinButton = builder.get_object(objectName);
        let handler = spinButton.connect('input', callbacks[handlerName]);
        spinButton.connect('unrealize', () => spinButton.disconnect(handler));
    });

    application.add_window(window);
    window.present();
});

application.run([]);
