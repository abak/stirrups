// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* Tree Store
 *
 * The GtkTreeStore is used to store data in tree form, to be
 * used later on by a GtkTreeView to display it. This demo builds
 * a simple GtkTreeStore and displays it. If you're new to the
 * GtkTreeView widgets and associates, look into the GtkListStore
 * example first.
 *
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GObject, Gtk } = imports.gi;
const ByteArray = imports.byteArray;
const directory = Gio.File.new_for_path('.');

// gtk_tree_view_insert_column_with_attributes JS implementation.
Gtk.TreeView.prototype.insertColumnWithAttributes = function(position, title, renderer, attributes) {
    let column = new Gtk.TreeViewColumn(
        this.fixedHeightMode ? { title, sizing: Gtk.TreeViewColumnSizing.FIXED } : { title }
    );

    column.pack_start(renderer, false);
    for (let attribute in attributes)
        column.add_attribute(renderer, attribute, attributes[attribute]);

    return this.insert_column(column, position);
};

// { month: [{ name, worldwide }, ...], ... }
const holidays = JSON.parse(
    ByteArray.toString(
        directory.get_child('holidays.json').load_contents(null)[1]
    )
);

const persons = [
    { name: "Bob", american: true, holidays: new Set([
        "New Years Day", "Presidential Inauguration", "Martin Luther King Jr. day",
        "Presidents' Day", "Memorial Day", "Flag Day", "Independence Day",
        "Columbus Day", "Veterans' Day", "Thanksgiving", "Christmas",
    ]) },
    { name: "Alice", american: false, holidays: new Set([
        "New Years Day", "Christmas",
    ]) },
    { name: "John", american: true, holidays: new Set([
        "Valentine's Day",
    ]) },
];

const Column = {
    HOLIDAY_NAME: 0,
    VISIBLE: 1,
    WORLDWIDE: 2,
    FIRST_PERSON: 3,
};

let store = Gtk.TreeStore.new(
    [GObject.TYPE_STRING, GObject.TYPE_BOOLEAN, GObject.TYPE_BOOLEAN].concat(
        new Array(persons.length).fill(GObject.TYPE_BOOLEAN)
    )
);

for (let [month, children] of Object.entries(holidays)) {
    let iter = store.append(null);
    store.set(iter, [Column.HOLIDAY_NAME, Column.VISIBLE, Column.WORLDWIDE], [month, false, false]);

    for (let holiday of children) {
        let childIter = store.append(iter);
        store.set(childIter, [Column.HOLIDAY_NAME, Column.VISIBLE, Column.WORLDWIDE], [holiday.name, true, holiday.worldwide]);

        persons.forEach((person, index) => {
            store.set_value(childIter, Column.FIRST_PERSON + index, person.holidays.has(holiday.name));
        });
    }
}

let application = new Gtk.Application();

application.connect('activate', () => {
    let treeView = new Gtk.TreeView({
        model: store,
        vexpand: true,
    });
    treeView.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE);
    // Expand all rows after the tree view widget has been realized.
    treeView.connect('realize', treeView => treeView.expand_all());

    // Column for holiday names.
    let renderer = new Gtk.CellRendererText({ xalign: 0 });
    let attributes = { 'text': Column.HOLIDAY_NAME };
    let columnOffset = treeView.insertColumnWithAttributes(-1, "Holiday", renderer, attributes);
    treeView.get_column(columnOffset - 1).set_clickable(true);

    persons.forEach((person, index) => {
        let renderer = new Gtk.CellRendererToggle({ xalign: 0 });
        renderer.connect('toggled', (renderer_, pathString) => {
            let iter = treeView.model.get_iter_from_string(pathString)[1];
            let item = treeView.model.get_value(iter, Column.FIRST_PERSON + index);
            treeView.model.set_value(iter, Column.FIRST_PERSON + index, !item);
        });

        let attributes = { 'active': Column.FIRST_PERSON + index, 'visible': Column.VISIBLE };
        if (!person.american)
            attributes['activatable'] = Column.WORLDWIDE;

        let columnOffset = treeView.insertColumnWithAttributes(-1, person.name, renderer, attributes);
        treeView.get_column(columnOffset - 1).set_clickable(true);
        treeView.get_column(columnOffset - 1).set_sizing(Gtk.TreeViewColumnSizing.FIXED);
    });

    let vbox = new Gtk.Box({
        orientation: Gtk.Orientation.VERTICAL,
        marginTop: 8, marginBottom: 8,
        marginStart: 8, marginEnd: 8,
        spacing: 8,
    });
    vbox.append(new Gtk.Label({
        label: "Holiday Card Planning Sheet",
    }));
    vbox.append(new Gtk.ScrolledWindow({
        hasFrame: true,
        child: treeView,
    }));

    new Gtk.Window({
        application, title: "Tree Store",
        defaultWidth: 650, defaultHeight: 400,
        child: vbox,
    }).present();
});

application.run([]);
