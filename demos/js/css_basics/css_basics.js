// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

/* CSS Basics
 *
 * GTK themes are written using CSS. Every widget is build of multiple items
 * that you can style very similarly to a regular website.
 */

imports.gi.versions['Gtk'] = '4.0';
const { Gio, GLib, Gtk, Pango } = imports.gi;
const ByteArray = imports.byteArray;
const directory = Gio.File.new_for_path('.');

let application = new Gtk.Application();

application.connect('activate', () => {
    let tagTable = new Gtk.TextTagTable();
    tagTable.add(new Gtk.TextTag({ name: 'warning', underline: Pango.Underline.SINGLE }));
    tagTable.add(new Gtk.TextTag({ name: 'error', underline: Pango.Underline.ERROR }));

    let buffer = new Gtk.TextBuffer({ tagTable });
    let provider = new Gtk.CssProvider();

    buffer.connect('changed', buffer => {
        buffer.remove_all_tags(buffer.get_start_iter(), buffer.get_end_iter());
        provider.load_from_data(buffer.text);
    });

    provider.connect('parsing-error', (provider, section, error) => {
        let startLocation = section.get_start_location();
        let endLocation = section.get_end_location();

        let startIter = buffer.get_iter_at_line_index(startLocation.lines, startLocation.line_bytes)[1];
        let endIter = buffer.get_iter_at_line_index(endLocation.lines, endLocation.line_bytes)[1];

        let tagName = GLib.quark_to_string(error.domain) == 'gtk-css-parser-warning-quark' ? 'warning' : 'error';
        buffer.apply_tag_by_name(tagName, startIter, endIter);
    });

    try {
        let [success_, contents] = directory.get_child('css_basics.css').load_contents(null);
        buffer.set_text(ByteArray.toString(contents), -1);
    } catch(e) {
        logError(e);
    }

    let resetProvider = new Gtk.CssProvider();
    resetProvider.load_from_file(directory.get_child('reset.css'));

    let textView = new Gtk.TextView({ buffer });
    let container = new Gtk.ScrolledWindow({ child: textView });
    let window = new Gtk.Window({
        application,
        title: "CSS Basics",
        defaultWidth: 400,
        defaultHeight: 300,
        child: container
    });

    Gtk.StyleContext.add_provider_for_display(window.display, resetProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
    Gtk.StyleContext.add_provider_for_display(window.display, provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION + 1);

    window.present();
});

application.run([]);
