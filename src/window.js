/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported Window */

const { Gio, GLib, GObject, Gtk } = imports.gi;
const { Demo } = imports.demo;

// Register the HighlightBuffer class for builder.
void imports.buffer;

const getIsDark = function(styleContext) {
    let color = styleContext.get_color();
    return (color.red + color.green + color.blue) > 3 / 2;
};

const settings = new Gio.Settings({ schemaId: pkg.name });

const Model = GObject.registerClass({
    GTypeName: 'StirrupsModel',
}, class Model extends Gtk.SingleSelection {
    _init(params) {
        this._searchNeedles = [];
        this.language = params.language || Demo.Language.JS;

        let listmodel = this._createDemoModel();
        let treemodel = Gtk.TreeListModel.new(listmodel, false, settings.get_boolean('auto-expand'), demo => demo.childrenModel || null);
        this._filterModel = Gtk.FilterListModel.new(treemodel, null);
        this._filter = Gtk.CustomFilter.new(this._demoFilterByName.bind(this));
        this._filterModel.set_filter(this._filter);

        super._init({ model: this._filterModel });
    }

    set searchNeedles(searchNeedles) {
        this._searchNeedles = searchNeedles;
        this._filter.changed(Gtk.FilterChange.DIFFERENT);
    }

    _demoFilterByName(row) {
        // Show all items if search is empty
        if (!this._searchNeedles || !this._searchNeedles[0])
            return true;

        // Show a row if itself of any parent matches
        for (let parent = row; parent; parent = parent.get_parent()) {
            let demo = parent.get_item();
            if (demo.matches(this._searchNeedles))
                return true;
        }

        // Show a row if any child matches
        let childrenModel = row.get_item().childrenModel;
        for (let i = 0; childrenModel && childrenModel.get_item(i); i++) {
            let demo = childrenModel.get_item(i);
            if (demo.matches(this._searchNeedles))
                return true;
        }

        return false;
    }

    _createDemoModel() {
        let store = new Gio.ListStore(Demo.$gtype);

        Demo.getDemos(this.language).forEach(demo => {
            store.append(demo);

            (demo.children || []).forEach(childDemo => {
                if (!demo.childrenModel)
                    demo.childrenModel = new Gio.ListStore(Demo.$gtype);
                demo.childrenModel.append(childDemo);
            });
        });

        return store;
    }
});

Gtk.Window.set_default_icon_name(pkg.name);
var Window = GObject.registerClass({
    GTypeName: 'StirrupsWindow',
    Template: `resource://${pkg.resourceBasePath}/ui/main.ui`,
    InternalChildren: ['notebook', 'info-textview', 'listview', 'searchbar', 'search-entry', 'source-textview'],
}, class Window extends Gtk.ApplicationWindow {
    _init(params) {
        super._init(params);

        let cssProvider = new Gtk.CssProvider();
        cssProvider.load_from_data('textview.monospace { font-size: calc(1em - 1pt); }');
        Gtk.StyleContext.add_provider_for_display(this.get_display(), cssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        let action = new Gio.SimpleAction({ name: 'run' });
        action.connect('activate', this._activateRun.bind(this));
        this.add_action(action);

        this._notebook.connect('switch-page', this._onPageSwitched.bind(this));
        this._listview.connect('activate', this._onListviewActivated.bind(this));
        this._searchbar.connect('notify::search-mode-enabled', this._clearSearch);
        this._searchbar.set_key_capture_widget(this);

        this._search_entry.connect('search-changed', this._onSearchChanged.bind(this));

        let factory = new Gtk.SignalListItemFactory();
        factory.connect('setup', this._onListItemFactorySetup);
        factory.connect('bind', this._onListItemFactoryBound);
        this._listview.set_factory(factory);

        this._models = {};

        // Not to bother with stack page widgets
        // The stack is here to switches the language models
        let stack = new Gtk.Stack();
        stack.connect('notify::visible-child-name', this._onLanguageChanged.bind(this));
        stack.add_titled(new Gtk.Box(), Demo.Language.JS, "JavaScript");
        stack.add_titled(new Gtk.Box(), Demo.Language.PYTHON, "Python");
        let switcher = new Gtk.StackSwitcher({ stack });
        this.get_titlebar().set_title_widget(switcher);
    }

    _onListItemFactorySetup(_factory, listItem) {
        let expander = new Gtk.TreeExpander({ child: new Gtk.Label() });
        let controller = new Gtk.GestureClick();
        controller.connect('pressed', (controller, n) => {
            let row = controller.get_widget().get_list_row();
            if (row.expandable && n == 1)
                row.expanded = !row.expanded;
        });
        expander.add_controller(controller);
        expander.action_set_enabled('listitem.toggle-expand', false);
        listItem.set_child(expander);
    }

    _onListItemFactoryBound(_factory, listItem) {
        let row = listItem.get_item();
        let demo = row.get_item();
        listItem.set_selectable(!row.expandable);
        listItem.get_child().set_list_row(row);
        listItem.get_child().get_child().set_label(demo.title);
    }

    _onSelectedItemChanged() {
        let row = this._model.get_selected_item();
        this._notebook.set_sensitive(!!row);

        if (!row)
            return;

        for (let i = this._notebook.get_n_pages() - 1; i > 1; i--)
            this._notebook.remove_page(i);

        let demo = row.get_item();

        this._info_textview.get_buffer().set_text(demo.sourceInfo, -1);
        let start = this._info_textview.get_buffer().get_iter_at_line(0)[1];
        let end = this._info_textview.get_buffer().get_iter_at_line(1)[1];
        this._info_textview.get_buffer().apply_tag_by_name('title', start, end);
        this._source_textview.get_buffer().set_text('', -1);
        this._source_textview.get_buffer().insertWithHighlight(
            this._source_textview.get_buffer().get_start_iter(),
            demo.sourceCode,
            getIsDark(this._source_textview.get_style_context()),
            demo.entryPoint
        );

        demo.depFilenames.forEach(filename => {
            this._notebook.append_page(new Gtk.ScrolledWindow(), new Gtk.Label({ label: filename }));
        });
    }

    _onLanguageChanged(stack) {
        let language = stack.get_visible_child_name();
        if (!this._models[language]) {
            this._models[language] = new Model({ language });
            this._models[language].connect('notify::selected-item', this._onSelectedItemChanged.bind(this));
        }

        this._model = this._models[language];
        this._listview.set_model(this._model);
        this._onSearchChanged();
        this._onSelectedItemChanged();
    }

    _onSearchChanged() {
        if (this._search_entry.get_text())
            this._model.searchNeedles = GLib.str_tokenize_and_fold(this._search_entry.get_text(), null)[0];
        else
            this._model.searchNeedles = [];
    }

    _clearSearch(searchbar) {
        if (!searchbar.get_search_mode())
            searchbar.get_child().set_text("");
    }

    _onListviewActivated(listview, position) {
        let row = listview.get_model().get_item(position);
        if (row.expandable)
            row.expanded = !row.expanded;
        else
            this._activateRun();
    }

    _onPageSwitched(notebook, pageWidget, index) {
        if (index <= 1 || pageWidget.filled)
            return;

        pageWidget.filled = true;

        let row = this._listview.get_model().get_selected_item();
        let demo = row.get_item();
        let filename = notebook.get_tab_label(pageWidget).get_label();
        let contents = demo.getDepContents(filename);

        if (!contents) {
            pageWidget.set_child(new Gtk.Label({ label: "The contents cannot be displayed" }));
        } else if (contents.text) {
            let builder = Gtk.Builder.new_from_resource(`${pkg.resourceBasePath}/ui/dep-page.ui`);
            let textview = builder.get_object('textview');
            textview.get_buffer().insertWithHighlight(
                textview.get_buffer().get_start_iter(),
                contents.text,
                getIsDark(textview.get_style_context()),
                filename
            );
            pageWidget.set_child(textview);
        } else if (contents.imageFile) {
            pageWidget.set_child(new Gtk.Picture({ file: contents.imageFile, halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER }));
        } else if (contents.videoFile) {
            pageWidget.set_child(new Gtk.Video({ file: contents.videoFile, loop: true }));
        } else if (contents.imageFiles) {
            let grid = new Gtk.FlowBox({ selection_mode: Gtk.SelectionMode.NONE, valign: Gtk.Align.START });
            pageWidget.set_child(grid);
            contents.imageFiles.forEach(file => {
                let picture = new Gtk.Picture({ file, halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER });
                let box = new Gtk.Box({ orientation: Gtk.Orientation.VERTICAL });
                box.append(new Gtk.ScrolledWindow({ child: picture }));
                box.append(new Gtk.Label({ label: file.get_basename() }));
                grid.insert(box, -1);
            });
        }
    }

    _activateRun() {
        let row = this._listview.get_model().get_selected_item();
        let demo = row.get_item();
        demo.run();
    }
});
