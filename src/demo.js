/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported Demo */

const { Gio, GLib, GObject } = imports.gi;
const { byteArray: ByteArray, format: Format } = imports;

const IMAGE_CONTENT_TYPES = ['image/'];
const TEXT_CONTENT_TYPES = ['text/', 'application/javascript', 'application/json', 'application/x-gtk-builder', 'application/xml'];
const VIDEO_CONTENT_TYPES = ['video/'];

const parseDirectory = function(directory, excluded) {
    let files = { collections: [], images: [], others: [], texts: [], videos: [] };
    if (!directory)
        return files;

    try {
        let enumerator = directory.enumerate_children('standard::content-type', Gio.FileQueryInfoFlags.NONE, null);
        let fileInfo;

        while (fileInfo = enumerator.next_file(null)) {
            let filename = enumerator.get_child(fileInfo).get_basename();
            if (filename == excluded)
                continue;

            let contentType = fileInfo.get_content_type();
            if (TEXT_CONTENT_TYPES.some(string => contentType.startsWith(string)))
                files.texts.push(filename);
            else if (IMAGE_CONTENT_TYPES.some(string => contentType.startsWith(string)))
                files.images.push(filename);
            else if (VIDEO_CONTENT_TYPES.some(string => contentType.startsWith(string)))
                files.videos.push(filename);
            else if (contentType == 'inode/directory')
                files.collections.push(filename);
            else
                files.others.push(filename);
        }

        enumerator.close(null);
    } catch(e) {
        if (!directory.query_exists(null))
            log(`${directory.get_path()} does not exist`);
        else
            logError(e);
    }

    for (key in files)
        files[key].sort();

    return files;
}

// Handle the demo logs
const spawnCommandLineAsync = function(workingDirectory, commandLine) {
    let success_, argv, pid, stdin, stdout, stderr;

    try {
        [success_, argv] = GLib.shell_parse_argv(commandLine);
        [success_, pid, stdin, stdout, stderr] = GLib.spawn_async_with_pipes(workingDirectory, argv, null, GLib.SpawnFlags.SEARCH_PATH,  null);
    } catch (e) {
        logError(e);
        return;
    }

    GLib.close(stdin);
    let outUnixStream = new Gio.UnixInputStream({ fd: stdout, close_fd: true });
    let errUnixStream = new Gio.UnixInputStream({ fd: stderr, close_fd: true });
    let outDataStream = new Gio.DataInputStream({ base_stream: outUnixStream, close_base_stream: true });
    let errDataStream = new Gio.DataInputStream({ base_stream: errUnixStream, close_base_stream: true });

    let read = dataStream => {
        dataStream.read_line_async(GLib.PRIORITY_LOW, null, (dataStream, res) => {
            let [line, length_] = dataStream.read_line_finish_utf8(res);
            if (line !== null) {
                if (line.trim())
                    log(line);
                read(dataStream);
            } else {
                dataStream.close(null);
            }
        });
    };

    read(outDataStream);
    read(errDataStream);
};

const Language = { JS: 'js', PYTHON: 'python' };

const getDemos = function(language) {
    let file = Gio.File.new_for_uri(`resource://${pkg.resourceBasePath}/demos`).get_child(language).get_child('demos.json');
    let [success_, contents] = file.load_contents(null);
    let demoData = JSON.parse(ByteArray.toString(contents));
    let command = demoData.command;
    let demos = [];

    demoData.demos.forEach(object => {
        let { title, keywords = null, name = null, entryPoint = null, command: demoCommand, children } = object;
        let demo = new Demo({
            language,
            title, keywords, name, entryPoint,
            command: demoCommand || command,
        });
        demos.push(demo);

        children?.forEach(childObject => {
            let { title, keywords = null, name, entryPoint, command: childDemoCommand } = childObject;
            let childDemo = new Demo({
                language,
                title, keywords, name, entryPoint,
                command: childDemoCommand || demoCommand || command,
            });

            (demo.children ?? (demo.children = [])).push(childDemo);
        });
    });

    return demos;
}

var Demo = GObject.registerClass({
    GTypeName: 'StirrupsDemo',
    Properties: {
        'title': GObject.ParamSpec.string(
            'title', "Title", "The demo title", GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, null
        ),
        'keywords': GObject.ParamSpec.boxed(
            'keywords', "Keywords", "The demo keywords", GObject.ParamFlags.READWRITE, GObject.type_from_name('GStrv')
        ),
        'language': GObject.ParamSpec.string(
            'language', "Language", "The demo language", GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY, null
        ),
        'name': GObject.ParamSpec.string(
            'name', "Name", "The name of the folder that contains the demo files", GObject.ParamFlags.READWRITE, null
        ),
        'entry-point': GObject.ParamSpec.string(
            'entry-point', "Entry point", "The demo entry point", GObject.ParamFlags.READWRITE, null
        ),
        'command': GObject.ParamSpec.string(
            'command', "Command", "The command to launch the demo with", GObject.ParamFlags.READWRITE, null
        ),
    },
}, class _Demo extends GObject.Object {
    static Language = Language;
    static getDemos = getDemos;

    get directory() {
        if (!this.name)
            return null;

        return Gio.File.new_for_path(pkg.pkgdatadir).get_child('demos').get_child(this.language).get_child(this.name);
    }

    matches(searchNeedles) {
        if (!this.title)
            return false;

        // Show only if the name matches every needle
        for (let i = 0; searchNeedles[i]; i++) {
            if (this.title.toLowerCase().includes(searchNeedles[i]))
                continue;

            if (this.keywords.length) {
                let found = false;
                for (let j = 0; !found && this.keywords[j]; j++)
                    found = this.keywords[j].toLowerCase().includes(searchNeedles[i]);
                if (found)
                    continue;
            }

            return false;
        }

        return true;
    }

    get sourceText() {
        if (!this.entryPoint || !this.directory)
            return '';

        if (!this._sourceText) {
            try {
                let [success_, contents] = this.directory.get_child(this.entryPoint).load_contents(null);
                // Strip SPDX.
                this._sourceText = ByteArray.toString(contents).replace(/(^.*SPDX-.*$)/gm, '');
            } catch(e) {
                logError(e);
                this._sourceText = '';
            }
        }

        return this._sourceText;
    }

    get sourceInfo() {
        if (!this._sourceInfo) {
            let info;

            // Extract info comment and strip comment characters
            if (this.language == Language.JS) {
                let found = this.sourceText.match(/\/\*[\s\S]*?\*\/|\/\/.*/);
                info = found ? found[0] : '';
                info = info.replace(/(^\s*[\/\*]+)|([\/\*]+\s*$)/gm, '');
            } else if (this.language == Language.PYTHON) {
                let found = this.sourceText.match(/"""[\s\S]*?"""|#.*/);
                info = found ? found[0] : '';
                info = info.replace(/(^\s*""")|("""\s*$)/gm, '');
                info = info.replace(/(^\s*[#]+)|([#]+\s*$)/gm, '');
            }

            // Remove empty lines and join full lines
            let lines = info.split(/\r\n|\r|\n/g);
            while (lines.length && !lines[0])
                lines.shift();
            while (lines.length && !lines[lines.length - 1])
                lines.pop();
            this._sourceInfo = lines.map(s => s.trim() ? s.trim() + ' ' : '\n').join('');
        }

        return this._sourceInfo;
    }

    get sourceCode() {
        if (!this._sourceCode) {
            // Strip info and top empty lines
            if (this.language == Language.JS)
                this._sourceCode = this.sourceText.replace(/(^\s*\/\*[\s\S]*?\*\/|\/\/.*)\s*/, '');
            else if (this.language == Language.PYTHON)
                this._sourceCode = this.sourceText.replace(/(^\s*"""[\s\S]*?"""|#.*)\s*/, '');
        }

        return this._sourceCode;
    }

    get deps() {
        if (!this._deps)
            this._deps = parseDirectory(this.directory, this.entryPoint);

        return this._deps;
    }

    get depFilenames() {
        return this.deps.texts.concat(this.deps.images, this.deps.videos, this.deps.collections, this.deps.others);
    }

    getDepContents(filename) {
        let contents = null;
        let file = this.directory.get_child(filename);

        if (this.deps.texts.includes(filename)) {
            try {
                let [success_, fileContents] = file.load_contents(null);
                // Strip SPDX.
                contents = { text: ByteArray.toString(fileContents).replace(/(^.*SPDX-.*$)/gm, '').trim() };
            } catch(e) {
                logError(e);
            }
        } else if (this.deps.images.includes(filename)) {
            contents = { imageFile: file };
        } else if (this.deps.videos.includes(filename)) {
            contents = { videoFile : file };
        } else if (this.deps.collections.includes(filename)) {
            let { images } = parseDirectory(file);
            contents = { imageFiles: images.map(filename => file.get_child(filename)) };
        }

        return contents;
    }

    run() {
        let missingElement = !this.command && "a command" || !this.directory && "a directory name" || !this.entryPoint && "an entry point";
        if (missingElement)
            throw new Error(`Trying to run the "${this.title}" demo without providing ${missingElement}`);

        // Replace "${libdir}" with pkg.libdir
        let command = this.command.replace(/\$\{(\w*)\}/g, (match_, p1) => {
            if (!pkg[p1])
                throw new Error(`"${p1}" variable not found.\nSupported variables:\n${JSON.stringify(pkg, null, 2)}`);

            return pkg[p1];
        });

        // Replace "%s" with this.entryPoint
        let commandLine = Format.vprintf(command, [this.entryPoint]);
        let workingDirectory = this.directory.get_path();

        spawnCommandLineAsync(workingDirectory, commandLine);
    }
});
